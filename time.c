/*
 * tine.c   显示系统时间
 *
 * Copyright (C) 2014, 2018   Caipenghui   蔡鹏辉   All Rights Reserved.
 *
 * Email: <Caipenghui_c@163.com>
 *
 *   ∧_∧::
 *  (´･ω･`)::
 *  /⌒　　⌒)::
 * /へ_＿  / /::
 * (＿＼＼  ﾐ)/::
 * ｜ `-イ::
 * /ｙ　 )::
 * /／  ／::
 * ／　／::
 * (　く:::
 * |＼ ヽ:::
 */

#include <stdio.h>
#include <time.h>

int main(void)
{
	time_t t;
	time(&t);
	printf("Today's date and time: %s", ctime(&t));
	return 0；
}
